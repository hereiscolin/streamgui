streamgui
=========
An [Erlang](https://www.erlang.org) web frontend for [streamlink](https://streamlink.github.io) and [Twitch](https://www.twitch.tv).

![](imgs/streamgui-screen.png)

Details
--------
A [cowboy server](https://ninenines.eu/docs/en/cowboy/2.0/guide/) runs in the background to serve web content and host a REST API. When the main page is loaded, an API request is made to [Twitch](https://twitch.tv) for the Top 100 streams. Each stream has the option of viewing with streamlink or forwarding directly to Twitch.

Prerequisites
-----------------
This application expects to read a [streamlink](https://github.com/streamlink/streamlink) configuration file. You can install `streamlink` through homebrew with the following:

```
brew install streamlink
```

Build and Run
---------------
`streamgui` is built using `rebar3`:

    rebar3 escriptize
    # An executable should now be available in the local directory as "streamgui"
    ./streamgui

After running the above, visit port `8080` on your [local machine](http://localhost:8080).

`CTRL-C` to exit or terminate the running application.

Configuration
--------------
There are a couple configuration options for `streamgui`:

1. Twitch auth
1. `streamlink` config file path
1. Listening port for the web server
1. (TODO) Location of `streamlink` application

### Twitch Configuration

Twitch requires a `Client-ID` to access the stream API and an OAuth token to view streams through `streamlink`. Both of these
tokens can be set in the [streamlink configuration file](https://streamlink.github.io/cli.html#configuration-file) and will 
be used correctly by `streamgui`.

To create the `Client-ID`, log in to Twitch's [Developer site](https://dev.twitch.tv) and create
a new application (`Dashboard` -> `Applications` -> `Register Your Application`). After filling in some details and creating a new application, copy the `Client ID` and add it to the streamlink config file.

![](imgs/twitch-client-id.png)

Add `Client-ID` to the expected config file:

```
echo "http-header=Client-ID=<CLIENT ID HERE>" >> ~/.streamlinkrc
```

For the OAuth token, this will be the `Client Secret`. Under `Client Secret`, currently below the _Client ID_ section, click
`New Secret`. Copy this value and add it to the streamlink config file.

![](imgs/twitch-new-secret.png)

```
echo "twitch-oauth-token=<CLIENT SECRET HERE>" >> ~/.streamlinkrc
```

### `streamlink` Config File Path
In addition to the default locations listed in the documentation, a file location can be specified through the `STREAMGUI_SLCONFIG` environment variable.

```
STREAMGUI_SLCONFIG=stream.conf ./streamgui
```

### Listening Port
By default, the web server listens on port `8080`. This can be changed through the `STREAMGUI_PORT` environment variable.

```
$ STREAMGUI_PORT=8090 ./streamgui
Web server available at http://localhost:8090
```
