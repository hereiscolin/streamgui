%%%-------------------------------------------------------------------
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------
-module(streamgui).

%% API
-export([main/1]).

-define(DEFAULT_PORT, 8080).
-define(ENV_HOME, "HOME").
-define(ENV_PORT, "STREAMGUI_PORT").
-define(ENV_XDG, "XDG_CONFIG_HOME").

main(_Args) ->
	%% start dependencies
	{ok, _} = application:ensure_all_started(ibrowse),
	{ok, _} = application:ensure_all_started(cowboy),

	%% env configs
	ListenPort = os:getenv(?ENV_PORT, ?DEFAULT_PORT),
	{ok, TwitchToken} = read_token(),

	%% cowboy config
	TokenMap = #{token => TwitchToken},
	Dispatch = cowboy_router:compile([
		{'_', [
			{"/api/v1/list/:page[/:game]", streamgui_list_handler, #{}},
			{"/api/v1/watch/:provider/:streamer", streamgui_watch_handler, TokenMap},
			{"/api/v1/watch", streamgui_watch_handler, TokenMap},
			{"/api/v1/config", streamgui_config_handler, #{}},
			{"/static/[...]", cowboy_static, {priv_dir, streamgui, "static"}},
			{"/", cowboy_static, {priv_file, streamgui, "index.html"}}
		]}
	]),

	%% start play service
	streamgui_playservice:start(TokenMap),
	%% start twitch service
	streamgui_twitch:start(TokenMap),

	%% start and monitor the web server
	%% TODO: does this cleanup streamlink?
	Result = do_run([{port, ListenPort}, {dispatch, Dispatch}], 0),
	io:format("exiting: ~p~n", [Result]),
	erlang:halt(0).

read_token() ->
	{OsFamily, _} = os:type(),
	{ok, Paths} = default_config_path(OsFamily),
	Paths2 = case os:getenv("STREAMGUI_SLCONFIG") of
				 false 	-> Paths;
				 Conf 	-> [Conf | Paths]
			 end,
	%% get only existing files
	FileList = lists:filtermap(
		fun(FilePath) -> 
			AbsPath = filename:absname(FilePath),
			case filelib:is_regular(FilePath) of
				true -> {true, AbsPath};
				_ -> false
			end
		end, Paths2),
	read_config_files(FileList).

-spec read_config_files(Files :: [string()]) -> Result :: {'error', 'no_more_files'} | {'ok', string()}.
read_config_files([]) -> {error, no_more_files};
read_config_files([File | T]) ->
	PossibleToken = read_config_file(File),
	case PossibleToken of
		{error, _} 		  -> read_config_files(T);
		{ok, _}  = Result -> Result
	end.

-spec read_config_file(File :: string()) -> {'error', atom()} | {'ok', Token :: string()}.
read_config_file(FilePath) ->
	case file:read_file(FilePath) of
		{error, _} = E 	-> E;
		{ok, Data} 		->
			Lines = binary:split(Data, <<"\n">>, [global]),
			parse(Lines)
	end.

-spec do_run(list(), non_neg_integer()) -> {'error', 'server_exited'} | {'error', 'unknown_error'}.
do_run(Config, Attempts) ->
	ListenPort = proplists:get_value(port, Config),
	WebConfig = proplists:get_value(dispatch, Config),
	{ok, Pid} = cowboy:start_clear(streamgui_http_listener,
		[{port, ListenPort}],
		#{env => #{dispatch => WebConfig}}
	),
	io:format("Web server available at http://localhost:~p~n", [ListenPort]),
	%% wait for any messages from the link;
	%% exit if anything comes through
	Monitor = erlang:monitor(process, Pid),
	receive
		{'DOWN', Monitor, process, _, _} ->
			case Attempts < 3 of
				true ->
					io:format("web server failed; restarting web server (~p/3)...~n", [Attempts+1]),
					do_run(Config, Attempts+1);
				_ ->
					io:format("web server failed and max retries reached; exiting.~n"),
					exit({error, server_exited})
			end;
		_ ->
			exit({error, unknown_error})
	end.

-spec default_config_path(atom()) -> {'ok', [string()]}.
default_config_path(unix) ->
	Home = os:getenv(?ENV_HOME),
	XDGConfig = os:getenv(?ENV_XDG),
	UnixPath = [filename:join(Home, ".streamlinkrc")],
	Path = case XDGConfig of
			   false ->
				   UnixPath;
			   _ ->
				   [filename:join(XDGConfig, "streamlink/config") | UnixPath]
		   end,
	{ok, Path};
default_config_path(_) ->
	{ok, ["%APPDATA%\\streamlink\\streamlinkrc"]}.

-spec parse(list()) -> {'ok', binary()} | {'error', 'out_of_content'}.
parse([]) ->
	{error, out_of_content};
parse([<<"http-header=Client-ID=", ClientID/binary>> | _]) ->
	{ok, ClientID};
parse([_ | T]) ->
	parse(T).

%%%===================================================================
%%% Tests
%%%===================================================================
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
default_config_path_test_() ->
	%% test the different default path settings
	%% have to do this so the env changes do not collide
	{foreach,
		fun default_path_setup/0,
		fun default_path_clean/1,
		[fun test_path_unix/1, fun test_path_unix_with_xdg/1, fun test_path_other/1]
	}.

parse_test_() ->
	[test_parse_empty(), test_parse_id()].

default_path_clean(_) ->
	true = os:unsetenv(?ENV_HOME),
	true = os:unsetenv(?ENV_PORT),
	true = os:unsetenv(?ENV_XDG).

default_path_setup() ->
	true = os:putenv(?ENV_HOME, "aoeu").

test_path_unix(_) ->
	[
		?_assertEqual({ok, ["aoeu/.streamlinkrc"]}, default_config_path(unix))
	].

test_path_unix_with_xdg(_) ->
	true = os:putenv(?ENV_XDG, "xdg"),
	[
		?_assertEqual({ok, ["xdg/streamlink/config", "aoeu/.streamlinkrc"]}, default_config_path(unix))
	].

test_path_other(_) ->
	[
		?_assertEqual({ok, ["%APPDATA%\\streamlink\\streamlinkrc"]}, default_config_path(win32))
	].

test_parse_empty() ->
	Expected = {error, out_of_content},
	[
		?_assertEqual(Expected, parse([])),
		?_assertEqual(Expected, parse([1, 2, "a", <<"b">>]))
	].

test_parse_id() ->
	Expected = {ok, <<"client">>},
	Data = <<"http-header=Client-ID=client">>,
	[
		?_assertEqual(Expected, parse([1, Data])),
		?_assertEqual(Expected, parse([Data])),
		?_assertEqual(Expected, parse([1, 3, "a", Data, <<"hi">>]))
	].

-endif.