%%%-------------------------------------------------------------------
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------
-module(streamgui_player).

%% API
-export([start/1]).

%%%===================================================================
%%% API
%%%===================================================================
start({_Command, _Args} = Input) ->
	spawn(fun() ->
		process_flag(trap_exit, true), %% need this to detect failures in ext app (streamlink)
		Port = open_port({spawn, command(Input)}, [binary, {line, 255}]),
		do_read(Port)
	end).

%%%===================================================================
%%% Internal
%%%===================================================================
-spec do_read(Port :: pid() | port()) -> ok | {'error_port_exit', Reason :: string()}.
do_read(Port) ->
	receive
		{Port, {data, {eol, Data}}} -> 
			io:format("~s~n", [Data]),
			do_read(Port);
		{Port, {data, Data}} -> 
			io:format("~s", [Data]),
			do_read(Port);
		{'EXIT', Port, normal} ->
			% exit normal
			exit(normal);
		{'EXIT', Port, Reason} ->
			exit({error_port_exit, Reason});
		Any -> 
			io:format("Unknown message: ~p~n", [Any]),
			do_read(Port)
	end.

command({Command, B}) when is_binary(Command) ->
	command({binary_to_list(Command), B});
command({Command, Args}) ->
	CommandString = flat_string([Command, flat_string(Args)]),
	CommandString.

flat_string("") -> "";
flat_string(A) 	-> lists:flatten(lists:join(" ", A)).
