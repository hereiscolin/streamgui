-module(streamgui_twitch).
-behaviour(gen_server).

-define(TWITCH_URL, "https://api.twitch.tv/helix/streams?first=~w").
-define(GAME_URL, "https://api.twitch.tv/helix/games?~s").
-define(LIMIT, 100).
-define(STREAMS_KEY, <<"data">>).
-define(DEFAULT_STREAM, #{
    <<"display_name">> => <<"Unknown Streamer">>,
    <<"game">> => <<"0">>,
    <<"name">> => <<"Unknown Streamer">>,
    <<"provider">> => <<"Twitch">>,
    <<"title">> => <<"">>,
    <<"viewers">> => <<"0">>,
    <<"url">> => <<"">>}).
%% prefix of Twitch thumbnail URL before the username
-define(PREFIX_LEN, 52).
%% 73 is the length of the thumbnail URL minus the username
%% in other words: there are 73 static characters and the unknown
%% name is the dynamic part to be extracted
%% https://static-cdn.jtvnw.net/previews-ttv/live_user_-{width}x{height}.jpg = 73
-define(THUMB_CHAR_LEN, 73).

-export([init/1, start/0, start/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).
-export([get_streams/0]).

%%% Server functions
start() -> gen_server:start({local, ?MODULE}, ?MODULE, [], []).
start(Args) -> gen_server:start({local, ?MODULE}, ?MODULE, [Args], []).

init([]) -> {ok, #{token => "", games => #{}}};
init([State]) when is_map(State) ->
    State2 = State#{games => #{}},
    {ok, State2}.
 
handle_call({fetch}, _From, State) ->
    #{ token := Token, games := Games } = State,
    Streams = twitch_streams(Token),
    MissingGames = missing_games(Streams, Games),
    GameList = case ordsets:size(MissingGames) > 0 of
        true ->
            Games2 = get_games(Token, MissingGames),
            GameMap = lists:foldl(fun update_gamelist/2, Games, Games2),
            GameMap;
        _ -> Games
    end,
    StreamFunc = fun(X) -> add_game_name(X, GameList) end,
    Streams2 = lists:map(StreamFunc, Streams),
    {reply, Streams2, State#{games => GameList}};
handle_call(terminate, _From, State) ->
    {stop, normal, ok, State}.
 
handle_cast(Msg, State) ->
    io:format("Received cast: ~p~n", [Msg]),
    {noreply, State}.

handle_info(Msg, State) ->
    io:format("Unexpected message: ~p~n",[Msg]),
    {noreply, State}.

terminate(normal, _State) -> ok.

code_change(_OldVsn, State, _Extra) ->
    %% No change planned. The function is there for the behaviour,
    %% but will not be used. Only a version on the next
    {ok, State}.

get_streams() ->
    gen_server:call(?MODULE, {fetch}).

-spec http_get(Url :: string(), Headers :: list(tuple()), Function :: function()) -> any().
http_get(Url, Headers, Function) ->
    case ibrowse:send_req(Url, Headers, get) of
		{ok, "200", _Headers, Body} ->
            Function(http_to_json(Body));
		_A ->
			[]
	end.

%%--------------------------------------------------------------------
%% @private
%% @doc Fetch streams from Twitch and return a list of Stream types.
%%
%% @spec twitch_streams(Page, Game, Token) -> list()
%%
%% @end
%%--------------------------------------------------------------------
-spec twitch_streams(Token :: string()) -> [map()] | list().
twitch_streams(Token) ->
	Headers = [{"Client-ID", Token}],
	Url = io_lib:format(?TWITCH_URL, [?LIMIT]),
    Fun = fun(Content) ->
        Streams = maps:get(?STREAMS_KEY, Content),
        Streams2 = [map_to_stream(Stream) || Stream <- Streams],
        Streams2
    end,
    StreamList = http_get(Url, Headers, Fun),
    StreamList.

-spec get_games(Token :: string(), Games :: list()) -> list().
get_games(Token, Games) ->
	Headers = [{"Client-ID", Token}],
    Games2 = game_query_params(Games),
	Url = io_lib:format(?GAME_URL, [string:join(Games2, "&")]),
    Fun = fun(Content) ->
        Streams = maps:get(?STREAMS_KEY, Content),
        Streams
    end,
    GameList = http_get(Url, Headers, Fun),
    GameList.

-spec missing_games(Streams :: list(), Games :: map()) -> list(binary()).
missing_games(Streams, Games) when is_list(Streams), is_map(Games) ->
    MissingGames = [GameId || #{<<"game">> := GameId} <- Streams, maps:is_key(GameId, Games) =:= false],
    MissingGames2 = ordsets:from_list(MissingGames),
    MissingGames2.

-spec game_query_params(Games :: list()) -> [string()].
game_query_params(Games) ->
    game_query_params(Games, []).

-spec game_query_params(Games :: list(), Accum :: list()) -> list().
game_query_params([], Accum) ->
    Accum;
game_query_params([Id|T], Accum) when Id =/= <<"0">>, Id =/= <<>> ->
    Id2 = binary_to_list(Id),
    Param = lists:flatten(io_lib:format("id=~s", [Id2])),
    game_query_params(T, [Param | Accum]);
game_query_params([_|T], Accum) ->
    game_query_params(T, Accum).

-spec update_gamelist(Game :: map(), GameCache :: map()) -> map().
update_gamelist(Game, GameCache) when is_map(GameCache) ->
    #{ <<"id">> := Key, <<"name">> := Val } = Game,
    GameCache#{Key => Val}.

-spec add_game_name(Game :: map(), GameCache :: map()) -> map().
add_game_name(Game, GameCache) when is_map(Game), is_map(GameCache) ->
    #{ <<"game">> := GameId } = Game,
    Name = maps:get(GameId, GameCache, <<"">>),
    Game#{<<"game">> => Name}.

%%--------------------------------------------------------------------
%% @private
%% @doc Decode JSON text to erlang maps.
%%
%% @spec http_to_json(Body :: list() | binary()) -> map()
%%
%% @end
%%--------------------------------------------------------------------
-spec http_to_json(Body :: binary() | list()) -> map().
http_to_json(Body) when is_list(Body) ->
	http_to_json(list_to_binary(Body));
http_to_json(Body) when is_binary(Body) ->
	jsx:decode(Body, [return_maps]).

%%--------------------------------------------------------------------
%% @private
%% @doc Convert a map to a generic Stream type (map).
%%
%% @spec map_to_stream(Provider, Stream) -> GenericStream
%%
%% @end
%%--------------------------------------------------------------------
-spec map_to_stream(map()) -> map().
map_to_stream(Stream) when is_map(Stream) ->
    case maps:size(Stream) > 0 of
        true ->
            #{
                <<"user_name">> := Name,
                <<"title">> := Title,
                <<"game_id">> := Game,
                <<"viewer_count">> := Viewers,
                <<"thumbnail_url">> := Thumb
            } = Stream,
            Channel = channel_name(Thumb),
            S = #{
                <<"display_name">> => Name,
                <<"game">> => Game,
                <<"name">> => Channel,
                <<"provider">> => <<"Twitch">>,
                <<"title">> => Title,
                <<"viewers">> => Viewers,
                <<"url">> => <<"https://twitch.tv/", Channel/binary>>
            },
            S;
        _ -> ?DEFAULT_STREAM
    end.


%%--------------------------------------------------------------------
%% @private
%% @doc Extract the channel name from the thumbnail URL. 
%% For non-english streams, such as Korean broadcasters, the "user_name"
%% field does not match the real channel name contained in the thumbnail
%% URL.
%%
%% @spec channel_name(Url) -> ChannelName
%%
%% @end
%%--------------------------------------------------------------------
-spec channel_name(Url :: binary()) -> ChannelName :: binary().
channel_name(Url) ->
    Len = byte_size(Url),
    NameLength = Len - ?THUMB_CHAR_LEN,
    <<_Prefix:?PREFIX_LEN/binary, ChannelName:NameLength/binary, _/binary>> = Url,
    ChannelName.

%%%===================================================================
%%% Tests
%%%===================================================================
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
query_param_test_() -> [test_query_params()].

map_to_stream_test_() -> [test_streams()].

missing_games_test_() ->
    [test_missing_games_empty(), test_missing_games_errors(), test_missing_games()].

update_gamelist_test_() -> [test_update_gamelist()].

test_query_params() ->
    Expected = [ "id=4", "id=3" ],
    Empty = ordsets:from_list([]),
    Dupes = ordsets:from_list([<<"3">>, <<"4">>, <<"3">>]),
    Valid = ordsets:from_list([<<"3">>, <<"4">>]),
    Filtered1 = ordsets:from_list([<<"3">>, <<"">>, <<"4">>]),
    Filtered2 = ordsets:from_list([<<"3">>, <<"0">>, <<"4">>]),
    [
        ?_assertEqual([], game_query_params(Empty)),
        ?_assertEqual(Expected, game_query_params(Dupes)),
        ?_assertEqual(Expected, game_query_params(Valid)),
        ?_assertEqual(Expected, game_query_params(Filtered1)),
        ?_assertEqual(Expected, game_query_params(Filtered2))
    ].

test_streams() ->
	Expected = #{
		<<"display_name">> => <<"test User">>,
		<<"game">> => <<"test game">>,
		<<"name">> => <<"different_name">>,
		<<"provider">> => <<"Twitch">>,
		<<"title">> => <<"a test title">>,
		<<"viewers">> => 10,
		<<"url">> => <<"https://twitch.tv/different_name">>
	},
	SingleStream = #{
        <<"user_name">> => <<"test User">>,
        <<"title">> => <<"a test title">>,
		<<"game_id">> => <<"test game">>,
		<<"viewer_count">> => 10,
        <<"thumbnail_url">> => <<"https://static-cdn.jtvnw.net/previews-ttv/live_user_different_name-{width}x{height}.jpg">>
	},
	[
		?_assertEqual(?DEFAULT_STREAM, map_to_stream(#{})),
		?_assertEqual(Expected, map_to_stream(SingleStream))
	].

test_missing_games_empty() ->
    [?_assertEqual([], missing_games([], #{<<"1">> => <<"A Game">>}))].

test_missing_games_errors() ->
    [
        ?_assertError(function_clause, missing_games([], [])),
        ?_assertError(function_clause, missing_games(<<"foobar">>, #{})),
        ?_assertError(function_clause, missing_games([1, 2], [])),
        ?_assertError(function_clause, missing_games(3, #{})),
        ?_assertError(function_clause, missing_games([], 4))
    ].

-define(game(X), #{<<"game">> => X}).
test_missing_games() ->
    GameCache = #{"foobar" => "ok"},
    [
        ?_assertEqual([3], missing_games([?game(3)], #{})),
        ?_assertEqual([], missing_games([?game("foobar")], GameCache)),
        ?_assertEqual(["fizz"], missing_games([?game("fizz")], GameCache))
    ].

-define(idname(X, Y), #{<<"id">> => X, <<"name">> => Y}).
test_update_gamelist() ->
    GameCache = #{"hi" => "bye"},
    [
        ?_assertEqual(#{"foobar" => "bizz"}, update_gamelist(?idname("foobar", "bizz"), #{})),
        ?_assertEqual(GameCache, update_gamelist(?idname("hi", "bye"), #{})),
        ?_assertEqual(#{"hi" => "hello"}, update_gamelist(?idname("hi", "hello"), GameCache)),
        ?_assertEqual(GameCache#{"ciao" => "hello"}, update_gamelist(?idname("ciao", "hello"), GameCache))
    ].
-endif.