%%%-------------------------------------------------------------------
%%% @author cdcd
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 24. Jul 2018 9:29 AM
%%%-------------------------------------------------------------------
-module(streamgui_streams).
-author("cdcd").

-define(MIXER_URL, "https://mixer.com/api/v1/channels?limit=~w&page=~w&order=online:desc,viewersCurrent:desc&noCount=1").
-define(MIXER_CHANNEL, "https://mixer.com/~s").
-define(LIMIT, 100).
-define(MAXOFFSET, 1600).

%% API
-export([get_streams/2]).

%%%===================================================================
%%% API
%%%===================================================================
%%--------------------------------------------------------------------
%% @doc Fetches streams from providers and returns a list of streams
%%	sorted by viewer count (descending).
%%
%% @end
%%--------------------------------------------------------------------
-spec get_streams(Page, Game) -> list() when
	Page :: pos_integer() | binary() | list(),
	Game :: string().
get_streams(Page, G) when is_binary(Page) ->
	get_streams(binary_to_integer(Page), G);
get_streams(Page, G) when is_list(Page) ->
	get_streams(list_to_integer(Page), G);
get_streams(Page, Game) ->
	TwitchStreams = streamgui_twitch:get_streams(),
	Streams = TwitchStreams++mixer_streams(Page, Game),
	Sorted = sort_viewers(Streams),
	Sorted.

%%%===================================================================
%%% Internal
%%%===================================================================
%%--------------------------------------------------------------------
%% @private
%% @doc Return a list of streams sorted by viewer count (descending).
%%
%% @spec sort_viewers(Streams) -> [map()]
%%
%% @end
%%--------------------------------------------------------------------
-spec sort_viewers([map()]) -> [map()].
sort_viewers(Streams) ->
	Sorted = lists:sort(
		fun(#{<<"viewers">> := ViewersA}, #{<<"viewers">> := ViewersB}) ->
			ViewersB =< ViewersA
		end,
		Streams
	),
	Sorted.

%%--------------------------------------------------------------------
%% @private
%% @doc Fetch streams from mixer and return a list of Stream types.
%%
%% @spec mixer_streams(Page, Game) -> list()
%%
%% @end
%%--------------------------------------------------------------------
-spec mixer_streams(Page :: pos_integer(), Game :: string()) -> [map()] | list().
mixer_streams(Page, Game) ->
	Url = construct_url({mixer, Page, Game}),
	case ibrowse:send_req(Url, [], get) of
		{ok, "200", _Headers, Body} ->
			Json = http_to_json(Body),
			streams_to_list({mixer, Json});
		_A ->
			[]
	end.

%%--------------------------------------------------------------------
%% @private
%% @doc Decode JSON text to erlang maps.
%%
%% @spec http_to_json(Body :: list() | binary()) -> map()
%%
%% @end
%%--------------------------------------------------------------------
-spec http_to_json(Body :: binary() | list()) -> map().
http_to_json(Body) when is_list(Body) ->
	http_to_json(list_to_binary(Body));
http_to_json(Body) when is_binary(Body) ->
	jsx:decode(Body, [return_maps]).

%%--------------------------------------------------------------------
%% @private
%% @doc Convert a list of streams from a provider to a generic or
%%	normalized Stream type. This Stream type is an erlang map.
%%
%% @spec streams_to_list({twitch, list()} | {mixer, list()}) -> list()
%%
%% @end
%%--------------------------------------------------------------------
-spec streams_to_list(Params) -> list() when
	Params :: {twitch, list()} | {mixer, list()}.
streams_to_list({Provider, StreamList}) ->
	Streams = [stream_from_provider(Provider, Stream) || Stream <- StreamList],
	Streams.

%%--------------------------------------------------------------------
%% @private
%% @doc Convert a map to a generic Stream type (map).
%%
%% @spec stream_from_provider(Provider, Stream) -> GenericStream
%%
%% @end
%%--------------------------------------------------------------------
-spec stream_from_provider(twitch | mixer, map()) -> map().
stream_from_provider(twitch, Stream) ->
	#{
		<<"channel">> := #{
			<<"display_name">> := DisplayName,
			<<"name">> := Name,
			<<"status">> := Title,
			<<"url">> := Url
		},
		<<"game">> := Game,
		<<"viewers">> := Viewers
	} = Stream,
	S = #{
		<<"display_name">> => DisplayName,
		<<"game">> => Game,
		<<"name">> => Name,
		<<"provider">> => <<"Twitch">>,
		<<"title">> => Title,
		<<"viewers">> => Viewers,
		<<"url">> => Url
	},
	S;
stream_from_provider(mixer, Stream) ->
	#{
		<<"viewersCurrent">> := Viewers,
		<<"name">> := Title,
		<<"user">> := #{<<"username">> := Name},
		<<"type">> := GameType
	} = Stream,
	Game = case GameType of
			   #{<<"name">> := Value} -> Value;
			   _ -> <<"">>
		   end,
	Url = list_to_binary(io_lib:format(?MIXER_CHANNEL, [Name])),
	S = #{
		<<"display_name">> => Name,
		<<"game">> => Game,
		<<"name">> => Name,
		<<"provider">> => <<"mixer">>,
		<<"title">> => Title,
		<<"viewers">> => Viewers,
		<<"url">> => Url
	},
	S.

%%--------------------------------------------------------------------
%% @private
%% @doc Get the URL to fetch based on the provider, page, and game
%% 	name.
%%
%% @spec construct_url({twitch, Page, Game} | {mixer, Page, Game}) -> string()
%%
%% @end
%%--------------------------------------------------------------------
-spec construct_url(Params) -> Result when
	Twitch :: {twitch, pos_integer(), string()},
	Mixer :: {mixer, pos_integer(), string()},
	Params :: Twitch | Mixer,
	Result :: string().
construct_url({mixer, Page, _}) ->
	io_lib:format(?MIXER_URL, [?LIMIT, Page]).

%%%===================================================================
%%% Tests
%%%===================================================================
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
construct_url_test_() ->
	[test_url_no_game()].

streams_test_() ->
	[test_mixer_streams()].

sort_test_() ->
	[test_sort_viewers()].

test_url_no_game() ->
	MixerUrl = io_lib:format(?MIXER_URL, [?LIMIT, 0]),
	MixerUrl2 = io_lib:format(?MIXER_URL, [?LIMIT, 3]),
	[
		?_assertEqual(MixerUrl, construct_url({mixer, 0, undefined})),
		?_assertEqual(MixerUrl2, construct_url({mixer, 3, undefined}))
	].

% test_url_with_game() ->
% 	DefaultUrl = io_lib:format(?GAME_URL, [?LIMIT, 0, "Fortnite"]),
% 	OffsetUrl = io_lib:format(?GAME_URL, [?LIMIT, 200, "Fortnite"]),
% 	SpacesUrl = io_lib:format(?GAME_URL, [?LIMIT, 500, "Dota%202"]),
% 	[
% 		?_assertEqual(DefaultUrl, construct_url({twitch, 0, "Fortnite"})),
% 		?_assertEqual(OffsetUrl, construct_url({twitch, 2, "Fortnite"})),
% 		?_assertEqual(SpacesUrl, construct_url({twitch, 5, "Dota 2"}))
% 	].

test_mixer_streams() ->
	ExpectedWithType = #{
		<<"display_name">> => <<"testuser">>,
		<<"game">> => <<"test game">>,
		<<"name">> => <<"testuser">>,
		<<"provider">> => <<"mixer">>,
		<<"title">> => <<"a test title">>,
		<<"viewers">> => 10,
		<<"url">> => <<"https://mixer.com/testuser">>
	},
	ExpectedWithoutType = #{
		<<"display_name">> => <<"testuser">>,
		<<"game">> => <<"">>,
		<<"name">> => <<"testuser">>,
		<<"provider">> => <<"mixer">>,
		<<"title">> => <<"a test title">>,
		<<"viewers">> => 10,
		<<"url">> => <<"https://mixer.com/testuser">>
	},
	WithType = #{
		<<"viewersCurrent">> => 10,
		<<"name">> => <<"a test title">>,
		<<"user">> => #{<<"username">> => <<"testuser">>},
		<<"type">> => #{<<"name">> => <<"test game">>}
	},
	WithoutType = #{
		<<"viewersCurrent">> => 10,
		<<"name">> => <<"a test title">>,
		<<"user">> => #{<<"username">> => <<"testuser">>},
		<<"type">> => null
	},
	[
		?_assertEqual([], streams_to_list({mixer, []})),
		?_assertEqual([ExpectedWithType], streams_to_list({mixer, [WithType]})),
		?_assertEqual([ExpectedWithoutType], streams_to_list({mixer, [WithoutType]})),
		?_assertEqual([ExpectedWithType, ExpectedWithoutType], streams_to_list({mixer, [WithType, WithoutType]}))
	].

test_sort_viewers() ->
	Expected = [
		#{<<"viewers">> => 100, <<"name">> => <<"first">>},
		#{<<"viewers">> => 75, <<"name">> => <<"second">>},
		#{<<"viewers">> => 65, <<"name">> => <<"third">>},
		#{<<"viewers">> => 50, <<"name">> => <<"fourth">>},
		#{<<"viewers">> => 1, <<"name">> => <<"fifth">>},
		#{<<"viewers">> => 0, <<"name">> => <<"sixth">>}
	],
	RandList = fun(L) ->
		%% from SO:
		%% https://stackoverflow.com/questions/8817171/shuffling-elements-in-a-list-randomly-re-arrange-list-elements/8820501#8820501
		[X || {_, X} <- lists:sort([{rand:uniform(), N} || N <- L])]
			   end,
	L1 = RandList(Expected),
	L2 = RandList(Expected),
	L3 = RandList(Expected),
	L4 = lists:reverse(Expected),
	[
		?_assertEqual(Expected, sort_viewers(L1)),
		?_assertEqual(Expected, sort_viewers(L2)),
		?_assertEqual(Expected, sort_viewers(L3)),
		?_assertEqual(Expected, sort_viewers(L4))
	].
-endif.