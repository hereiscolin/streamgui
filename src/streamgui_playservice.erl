-module(streamgui_playservice).
-behaviour(gen_server).

-export([init/1, start/0, start/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).
-export([watch/1]).

-define(TWITCH_PREFIX, "https://www.twitch.tv/").

-type command() :: string().
-type binary_command() :: binary().
-type args() :: string().
-type binary_args() :: binary().

%%===================================================================
%% gen_server FUNCTIONS
%%===================================================================

start() -> gen_server:start({local, ?MODULE}, ?MODULE, [], []).
start(Args) -> gen_server:start({local, ?MODULE}, ?MODULE, [Args], []).

init([]) -> {ok, #{streams => [], monitors => []}};
init([State]) when is_map(State) -> {ok, #{streams => [], monitors => []}}.

handle_call(terminate, _From, State) ->
    {stop, normal, ok, State}.

handle_cast({start, {Command, Args}}, #{streams := Streams, monitors := Monitors} = State) ->
    CommandCall = {Command, [Args, "best"]},
    Pid = streamgui_player:start(CommandCall),
    Ref = monitor(process, Pid),
    Streams2 = [Pid | Streams],
    Monitors2 =  [Ref | Monitors],
    {noreply, State#{streams := Streams2, monitors := Monitors2}};
handle_cast(Msg, State) ->
    io:format("Received cast: ~p~n", [Msg]),
    {noreply, State}.

handle_info({'DOWN', Ref, process, Pid, Reason}, #{streams := Streams, monitors := Monitors} = State) ->
    Streams2 = lists:delete(Pid, Streams),
    Monitors2 = lists:delete(Ref, Monitors),
    ok = reason(Reason),
    {noreply, State#{streams := Streams2, monitors := Monitors2}};
handle_info(Msg, State) ->
    io:format("Unexpected message: ~p~n",[Msg]),
    {noreply, State}.

terminate(normal, _State) -> ok.

code_change(_OldVsn, State, _Extra) ->
    %% No change planned. The function is there for the behaviour,
    %% but will not be used. Only a version on the next
    {ok, State}.

%%===================================================================
%% APPLICATION FUNCTIONS
%%===================================================================

-spec watch({Command, Args}) -> Result when
    Command :: command() | binary_command(),
    Args :: args() | binary_args(),
    Result :: {'call', pid(), {'start', {Command, Args}}}.
watch({Command, Args}) when is_binary(Command), is_binary(Args) ->
    watch({binary_to_list(Command), binary_to_list(Args)});
watch({Command, Args}) ->
    %% 	PlayerArgs = [Command]++header(Provider2, State)++[StreamUrl, "best"],
    gen_server:cast(?MODULE, {start, {Command, Args}}).

%%%===================================================================
%%% Internal
%%%===================================================================
reason(normal) -> ok;
reason(Reason) ->
    io:format("streamlink failed: ~p~n", [Reason]).

%%%===================================================================
%%% Tests
%%%===================================================================
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
reason_test_() -> [test_reason_normal(), test_reason_dynamic()].
handle_info_test_() -> [test_info()].

test_reason_normal() -> [?_assertEqual(ok, reason(normal))].
test_reason_dynamic() -> [?_assertEqual(ok, reason("non-atom reason"))].

test_info() ->
    State = #{streams => [1, 2, 3], monitors => [a, b, c]},
    ExpectedState = #{streams => [1, 3], monitors => [a, b]},
    [
        % normal
        ?_assertEqual({noreply, ExpectedState}, handle_info({'DOWN', c, process, 2, normal}, State)),
        ?_assertEqual({noreply, State}, handle_info({'DOWN', d, process, 5, normal}, State)),
        % not normal / Reason
        ?_assertEqual({noreply, ExpectedState}, handle_info({'DOWN', c, process, 2, not_normal}, State)),
        ?_assertEqual({noreply, State}, handle_info({'DOWN', d, process, 5, not_normal}, State)),
        % unknown message
        ?_assertEqual({noreply, State}, handle_info("some message here", State))
    ].

-endif.
