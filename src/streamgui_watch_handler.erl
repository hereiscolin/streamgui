%%%-------------------------------------------------------------------
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------
-module(streamgui_watch_handler).

%% API
-export([init/2, allowed_methods/2, content_types_accepted/2, from_json/2]).

%%%===================================================================
%%% API
%%%===================================================================
init(Req, State) ->
	{cowboy_rest, Req, State}.

allowed_methods(Req, State) ->
	{[<<"POST">>], Req, State}.

content_types_accepted(Req, State) ->
	{[
		{{<<"application">>, <<"json">>, '*'}, from_json}
	], Req, State}.

from_json(Req, State) ->
	%% request content / body
	{ok, BodyContent, Req2} = cowboy_req:read_body(Req),
	BodyJson = jsx:decode(BodyContent, [return_maps]),

	%% streamlink command construction
	Command = maps:get(<<"command">>, BodyJson),
	StreamUrl = maps:get(<<"url">>, BodyJson),

	%% spawn streamlink in separate process to not block this handler.
	streamgui_playservice:watch({Command, StreamUrl}),

	%% return true without redirect.
	{true, Req2, State}.
