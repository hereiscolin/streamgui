%%%-------------------------------------------------------------------
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------
-module(streamgui_list_handler).


%% API
-export([init/2, allowed_methods/2, content_types_provided/2, to_json/2]).

%% Functions
init(Req, State) ->
    {cowboy_rest, Req, State}.

%% callbacks
allowed_methods(Req, State) ->
    {[<<"GET">>], Req, State}.

content_types_provided(Req, State) ->
    {[
        {{<<"application">>, <<"json">>, '*'}, to_json}
    ], Req, State}.

to_json(Req, State) ->
    Game = cowboy_req:binding(game, Req),
    Page = cowboy_req:binding(page, Req),
    Streams = streamgui_streams:get_streams(Page, Game),
    StreamsJson = jsx:encode(Streams),
    {StreamsJson, Req, State}.
