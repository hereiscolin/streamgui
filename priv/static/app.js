var streamerl = (function (streamerl) {
    var app = new Vue({
        el: "#app",
        data: {
            page: 0,
            streams: [],
            cmd: 'streamlink'
        },
        methods: {
            watch: function (stream_url) {
                this.$http.post('/api/v1/watch', {command: this.cmd, url: stream_url})
                    .then(function (response) {
                        if (!response.ok)
                            console.log(response);
                    });
            },
            get_game: function (game) {
                this.$http.get('/api/v1/list/' + this.page + '/' + game)
                    .then(function (response) {
                        this.streams = response.body;
                    });
            }
        },
        created: function () {
            this.$http.get('/api/v1/list/' + this.page)
                .then(function (response) {
                    this.streams = response.body;
                });
        }
    });
}(streamerl || {}));